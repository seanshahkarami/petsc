#include <petsc/private/dmnetworkimpl.h>  /*I  "petscdmnetwork.h"  I*/

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCICreateForElements"
/*@
  DMNetworkCICreateForElements - Creates a component iterator (CI) over the components in the element array.
@*/
PetscErrorCode DMNetworkCICreateForElements(PetscInt *elements,PetscInt numelements,PetscBool ownelements,DMNetworkCI *citr)
{
  PetscErrorCode ierr;
 
  PetscFunctionBegin;
  ierr = PetscMalloc1(1, citr);CHKERRQ(ierr);
  (*citr)->elements    = elements;
  (*citr)->numelements = numelements;
  (*citr)->ownelements = ownelements;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkVertexCICreate"
/*@
  DMNetworkVertexCICreate - Creates a component iterator over the components on each vertex.
@*/
PetscErrorCode DMNetworkVertexCICreate(DM network,DMNetworkCI *vci)
{
  PetscErrorCode ierr;
  PetscInt       v,vStart,vEnd,*elements,numelements;
  
  PetscFunctionBegin;
  ierr = DMNetworkGetVertexRange(network, &vStart, &vEnd);CHKERRQ(ierr);
  numelements = vEnd - vStart;
  ierr = PetscMalloc1(1, &elements);CHKERRQ(ierr);

  for (v = vStart; v < vEnd; v++) {
    elements[v-vStart] = v;
  }

  ierr = DMNetworkCICreateForElements(elements, numelements, PETSC_TRUE, vci);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkEdgeCICreate"
/*@
  DMNetworkEdgeCICreate - Creates a component iterator over the components on each edge.
@*/
PetscErrorCode DMNetworkEdgeCICreate(DM network,DMNetworkCI *eci)
{
  PetscErrorCode ierr;
  PetscInt       e,eStart,eEnd,*elements,numelements;

  PetscFunctionBegin;
  ierr = DMNetworkGetEdgeRange(network, &eStart, &eEnd);CHKERRQ(ierr);
  numelements = eEnd - eStart;
  ierr = PetscMalloc1(1, &elements);CHKERRQ(ierr);

  for (e = eStart; e < eEnd; e++) {
    elements[e-eStart] = e;
  }

  ierr = DMNetworkCICreateForElements(elements, numelements, PETSC_TRUE, eci);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCISetUp"
/*@
  DMNetworkCISetUp - Create and setup edge and vertex component iterators.
@*/
PetscErrorCode DMNetworkCISetUp(DM network)
{
  PetscErrorCode ierr;
  DMNetworkCI    eci,vci;
  DM_Network     *mynetwork = (DM_Network*)network->data;
  
  PetscFunctionBegin;
  ierr = DMNetworkEdgeCICreate(network,&eci);CHKERRQ(ierr);
  ierr = DMNetworkVertexCICreate(network,&vci);CHKERRQ(ierr);
  mynetwork->eci = eci;
  mynetwork->vci = vci;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCICreateForComponents"
/*@
  DMNetworkCICreateForComponents - Creates a component iterator over the components on each vertex and edge.  ???
@*/
PetscErrorCode DMNetworkCICreateForComponents(DM network,DMNetworkCI *ci)
{
  PetscInt       v,vStart,vEnd,e,eStart,eEnd,*elements,numelements;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = DMNetworkGetVertexRange(network, &vStart, &vEnd);CHKERRQ(ierr);
  ierr = DMNetworkGetEdgeRange(network, &eStart, &eEnd);CHKERRQ(ierr);
  numelements = (vEnd - vStart) + (eEnd - eStart);
  ierr = PetscMalloc1(numelements, &elements);CHKERRQ(ierr);

  /* copy vertex elements */
  for (v = vStart; v < vEnd; v++) {
    elements[v-vStart] = v;
  }

  /* copy edge elements */
  for (e = eStart; e < eEnd; e++) {
    elements[e-eStart+vEnd-vStart] = e;
  }

  ierr = DMNetworkCICreateForElements(elements, numelements, PETSC_TRUE, ci);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCIDestroy"
/*@
  DMNetworkCIDestroy - Free space taken by a DMNetworkCI
@*/
PetscErrorCode DMNetworkCIDestroy(DMNetworkCI *citr)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* destroy element array if we own it */
  ierr = PetscFree((*citr)->elements);CHKERRQ(ierr);
  ierr = PetscFree(*citr);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCIFirst"
/*@
  DMNetworkCIFirst - Points a component iterator to the first component on the network.
@*/
void DMNetworkCIFirst(DM network,DMNetworkCI ci)
{
  ci->index = -1;
  ci->component = 0;
  ci->numcomponents = 0;
  DMNetworkCINext(network,ci);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCIDone"
/*@
  DMNetworkCIDone - Checks if component iterator is done.
@*/
PetscInt DMNetworkCIDone(DMNetworkCI ci)
{
  return ci->index >= ci->numelements;
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCINext"
/*@
  DMNetworkCINext - Points a component iterator to the next component on the network.
@*/
void DMNetworkCINext(DM network,DMNetworkCI ci)
{
  DMNetworkComponentGenericDataType *datastart;

  while (!DMNetworkCIDone(ci)) {
    ci->component++;

    if (ci->component >= ci->numcomponents) {
      ci->component = 0;
      ci->index++;
    }

    if (!DMNetworkCIDone(ci)) {
      ci->element = ci->elements[ci->index];
      DMNetworkGetNumComponents(network, ci->element, &(ci->numcomponents));
      DMNetworkGetComponentTypeOffset(network, ci->element, ci->component, &(ci->type), &(ci->typeoffset));
      DMNetworkGetVariableOffset(network, ci->element, &(ci->varoffset));
      DMNetworkGetNumVariables(network, ci->element, &(ci->numvars));
      DMNetworkIsGhostVertex(network, ci->element, &(ci->ghost));

      DMNetworkGetComponentDataArray(network, &datastart);
      ci->data = datastart + ci->typeoffset;

      if (ci->numcomponents > 0) break;
    }
  }
}

#undef __FUNCT__
#define __FUNCT__ "EdgeCIFirst"
/*@
  EdgeCIFirst - Points an iterator to the first edge component on the network.
@*/
void EdgeCIFirst(DM network)
{
  DM_Network     *mynetwork = (DM_Network*)network->data;
  DMNetworkCI    ci = mynetwork->eci;
  DMNetworkCIFirst(network,ci);
}

#undef __FUNCT__
#define __FUNCT__ "VertexCIFirst"
/*@
  VertexCIFirst - Points an iterator to the first vertex component on the network.
@*/
void VertexCIFirst(DM network)
{
  DM_Network     *mynetwork = (DM_Network*)network->data;
  DMNetworkCI    ci = mynetwork->vci;
  DMNetworkCIFirst(network,ci);
}

#undef __FUNCT__
#define __FUNCT__ "EdgeCINext"
/*@
  EdgeCINext - Points an iterator to the next edge component on the network.
@*/
void EdgeCINext(DM network)
{ 
  DM_Network     *mynetwork = (DM_Network*)network->data;
  DMNetworkCI    ci = mynetwork->eci;
  DMNetworkCINext(network,ci);
}

#undef __FUNCT__
#define __FUNCT__ "VertexCINext"
/*@
  VertexCINext - Points an iterator to the next vertex component on the network.
@*/
void VertexCINext(DM network)
{ 
  DM_Network     *mynetwork = (DM_Network*)network->data;
  DMNetworkCI    ci = mynetwork->vci;
  DMNetworkCINext(network,ci);
}

#undef __FUNCT__
#define __FUNCT__ "EdgeCIDone"
/*@
  EdgeCIDone - Checks if edge iterator is done.
@*/
PetscInt EdgeCIDone(DM network)
{ 
  DM_Network     *mynetwork = (DM_Network*)network->data;
  DMNetworkCI    ci = mynetwork->eci;
  return ci->index >= ci->numelements;
}

#undef __FUNCT__
#define __FUNCT__ "VertexCIDone"
/*@
  VertexCIDone - Checks if vertex iterator is done.
@*/
PetscInt VertexCIDone(DM network)
{ 
  DM_Network     *mynetwork = (DM_Network*)network->data;
  DMNetworkCI    ci = mynetwork->vci;
  return ci->index >= ci->numelements;
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCINextOfType"
/*@
  DMNetworkCINextOfType - Points an iterator to the next component on the network of a specified type.
  This is useful for picking out a homogeneous sequence of components.
@*/
void DMNetworkCINextOfType(DM network,DMNetworkCI ci,PetscInt type)
{
  do {
    DMNetworkCINext(network,ci);
  }
  while (!DMNetworkCIDone(ci) && ci->type != type);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkCIGetInfo"
/*@
  DMNetworkCIGetInfo - Gets information about a given iterator
 
  Not Collective

  Input Parameter:
. itr - the iterator

  Output Parameters:
+ index - index 
. type - component type
. varoffset - varible offset
. ghost - ghost flag
- data - type data

@*/
PetscErrorCode DMNetworkCIGetInfo(DMNetworkCI itr,PetscInt *index,PetscInt *type,PetscInt *varoffset,PetscBool *ghost,void **data)
{
  PetscFunctionBegin;
  if (index)      *index      = itr->index;
  if (type)       *type       = itr->type;
  if (varoffset)  *varoffset  = itr->varoffset;
  if (ghost)      *ghost      = itr->ghost;
  if (data)       *data       = itr->data;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkEdgeCIGetInfo"
PetscErrorCode DMNetworkEdgeCIGetInfo(DM network,PetscInt *index,PetscInt *type,PetscInt *varoffset,PetscBool *ghost,void **data)
{
  DM_Network      *mynetwork = (DM_Network*)network->data;
  DMNetworkCI     itr = mynetwork->eci;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  ierr = DMNetworkCIGetInfo(itr,index,type,varoffset,ghost,data);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "DMNetworkVertexCIGetInfo"
PetscErrorCode DMNetworkVertexCIGetInfo(DM network,PetscInt *index,PetscInt *type,PetscInt *varoffset,PetscBool *ghost,void **data)
{
  DM_Network      *mynetwork = (DM_Network*)network->data;
  DMNetworkCI     itr = mynetwork->vci;
  PetscErrorCode  ierr;

  PetscFunctionBegin;
  ierr = DMNetworkCIGetInfo(itr,index,type,varoffset,ghost,data);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
